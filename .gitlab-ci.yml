workflow:
  rules:
    # Adapted from template Workflows/MergeRequest-Pipelines.gitlab-ci.yml
    # for merge request pipelines.
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG
      variables:
        # Use a different repository on default branch to ensure it is around
        # when it gets released. This repository is not safe to delete during a
        # release. It is, however, safe to delete if there is no pending or
        # running release pipeline. I.e. after the last release:* and
        # release-latest:* jobs finish.
        BUILD_IMAGE_REPOSITORY: "${CI_REGISTRY_IMAGE}/builds/default"


variables:
  DOCKER_DRIVER: overlay2
  DOCKER_VERSION: '20.10.6'
  DOCKER_BUILDKIT: '1'

  ALPINE_VERSION: '3.21.3'
  KUBECTL_VERSION: '1.32.2'
  HELM3_VERSION: '3.17.1'
  PLATFORMS: linux/amd64,linux/arm64

  # Build branch images on a special ref that can be deleted in bulk
  BUILD_IMAGE_REPOSITORY: "${CI_REGISTRY_IMAGE}/builds/branches"
  # Common to all builds
  BUILD_IMAGE_TAG: "${CI_COMMIT_SHA}-${HELM_VARIANT}"
  BUILD_IMAGE_NAME: "${BUILD_IMAGE_REPOSITORY}:${BUILD_IMAGE_TAG}"

stages:
  - build
  - test
  - prepare-release
  - release

################################################################################
# base jobs
################################################################################

.helm3:
  variables:
    HELM_VARIANT: helm3
    RELEASE_IMAGE_REPOSITORY: "${CI_REGISTRY_IMAGE}"
    RELEASE_IMAGE_TAG_SUFFIX: "helm-${HELM3_VERSION}-kube-${KUBECTL_VERSION}-alpine-${ALPINE_VERSION}"
    LATEST_IMAGE_NAME: "${CI_REGISTRY_IMAGE}:latest"

.build:
  stage: build
  image: "docker:${DOCKER_VERSION}"
  services:
    - "docker:${DOCKER_VERSION}-dind"
  before_script:
    # install buildx
    - mkdir -p ~/.docker/cli-plugins
    - wget https://github.com/docker/buildx/releases/download/v0.5.1/buildx-v0.5.1.linux-amd64 -O ~/.docker/cli-plugins/docker-buildx
    - chmod a+x ~/.docker/cli-plugins/docker-buildx
    # See https://www.docker.com/blog/multi-platform-docker-builds/
    - docker run --rm --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64

    # registry auth
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  rules:
    # already built and tested on default branch
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

.test:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  services:
    - name: "registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v1.32.2-k3s1"
      alias: k3s
  before_script:
    - wget k3s:8081 -O k3s.yaml
    - export KUBECONFIG="$PWD/k3s.yaml"
  rules:
    # already built and tested on default branch
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

.release:
  stage: release
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    # https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane_copy.md
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane cp "$BUILD_IMAGE_NAME" "${RELEASE_IMAGE_REPOSITORY}:${CI_COMMIT_TAG}-${RELEASE_IMAGE_TAG_SUFFIX}"
    - crane cp "$BUILD_IMAGE_NAME" "${RELEASE_IMAGE_REPOSITORY}:${RELEASE_IMAGE_TAG_SUFFIX}"
  rules:
    - if: $CI_COMMIT_TAG

.release-latest:
  extends: .release
  # environment and resource group prevent pushing outdated latest tags
  environment: ${HELM_VARIANT}
  resource_group: ${HELM_VARIANT}
  variables:
    RELEASE_IMAGE_NAME: $LATEST_IMAGE_NAME

################################################################################
# stage: build
################################################################################

build:helm3:
  extends: [.build, .helm3]
  script:
    - docker buildx create --use
    - docker buildx build
        --platform "$PLATFORMS"
        --build-arg "ALPINE_VERSION=$ALPINE_VERSION"
        --build-arg "HELM3_VERSION=$HELM3_VERSION"
        --build-arg "KUBECTL_VERSION=$KUBECTL_VERSION"
        --build-arg "SOURCE_URL=$CI_PROJECT_URL"
        --file Dockerfile
        --tag "$BUILD_IMAGE_NAME"
        --push
        .

################################################################################
# stage: test
################################################################################

test:helm3:
  extends: [.test, .helm3]
  needs: [build:helm3]
  script:
    - kubectl version
    - helm version

test:shellcheck:
  stage: test
  image: koalaman/shellcheck-alpine:stable
  needs: []
  script:
    - shellcheck *.sh
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

test:shfmt:
  stage: test
  image:
    name: mvdan/shfmt:v3.2.1-alpine
  needs: []
  script:
    - FORCE_COLOR=true shfmt -i 2 -ci -l -d *.sh
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

test:update-checksums:
  stage: test
  image: ubuntu
  needs: []
  before_script:
    - apt update
    - apt install -y sed curl
  script:
    - ./update-checksums.sh
    - if [ -n "$(git status --porcelain)" ]; then echo "Checksum mismatch detected"; exit 1; fi
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: on_success

################################################################################
# stage: prepare-release
################################################################################

prepare-release:
  stage: prepare-release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    # Prevent infinite release loop
    - if: $CI_COMMIT_TAG
      when: never
    # Always release on default branch. This can result in some extraneus
    # version bumps, e.g. for docs-only changes or manually run pipelines, but
    # those should be rare. As long as they are rare, ignoring them is not a
    # problem for downstream.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - echo "Running release_job for $TAG"
  release:
    tag_name: 'v0.$CI_PIPELINE_IID'
    description: 'v0.$CI_PIPELINE_IID'
    ref: '$CI_COMMIT_SHA'

################################################################################
# stage: release
################################################################################

release-tag:helm3:
  extends: [.release, .helm3]

release-latest:helm3:
  extends: [.release-latest, .helm3]
